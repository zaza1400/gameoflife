/**
 * @file    MainArguments.h
 * @author      Erik Ström
 * @date        October 2017
 * @version     0.1
*/

#ifndef GAMEOFLIFE_MAINARGUMENTS_H
#define GAMEOFLIFE_MAINARGUMENTS_H

#include "Globals.h"
#include "ScreenPrinter.h"
#include <sstream>
#include <string>

using namespace std;

/**
* @struct ApplicationValues
* @brief Holds parameters for application
* @details By default runSimulation is set to true
* and maxGenerations is set to 100.
*/
struct ApplicationValues {
    bool runSimulation = true;
    string evenRuleName, oddRuleName;
    int maxGenerations = 100;

};
/**
* @class BaseArgument
* @brief Base class for all the argument classes.
*/
class BaseArgument {
protected:
    /**
    * @brief The value of argument as a string.
    */
    const string argValue;

    /**
    * @brief inform the user that no value was provided for the argument
    */
    void printNoValue();

public:
    /**
    * @brief constructor for class
    * @details It only initializes the argValue
    * @param argValue A string holding the value of argument.
    */
    BaseArgument(string argValue) : argValue(argValue) {}
    /**
    * @brief the destructor of the class
    * @details It is virtual to let objects of derived classes
    * call their own destructor when being deleted through a pointer
    * to the base class.
    */
    virtual ~BaseArgument() {}

    /**
    * @brief The function that each derived class should define to execute.
    * @details This method should apply the argument to appValues or any other
    * execution scenario.
    * @param appValues A reference to ApplicationValues. the function most likely modifies this
    * @param value A null terminated char array containing the value of argument.
    */
    virtual void execute(ApplicationValues& appValues, char* value = nullptr) = 0;

    /**
    * @brief Gets the value of argument
    * @return string The value of argument
    */
    const string& getValue() { return argValue; }
};

/**
* @class HelpArgument
* @brief Implements the help argument.
* @details It prints the help screen upon execution.
*/
class HelpArgument : public BaseArgument {
public:
    /**
    * @brief the constructor of class
    * @details To construct the object, the constructor of base class
    * is called with argument "-h"
    */
    HelpArgument() : BaseArgument("-h") {}
    /**
    * @brief The destructor of class
    */
    ~HelpArgument() {}

    /**
    * @brief Executes the help argument.
    * @details It prints the help screen by using the ScreenPrinter
    * @param appValues A reference to ApplicationValues.
    * @param value A null terminated char array containing the value of argument.
    */
    void execute(ApplicationValues& appValues, char* value);
};
/**
* @class GenerationsArgument
* @brief Implements the generations argument.
* @details The generations argument controls maximum 
* number of generations to simulates. This class 
* handles the execution of this argument.
*/

class GenerationsArgument : public BaseArgument {
public:
    /**
    * @brief the constructor of class
    * @details To construct the object, the constructor of base class
    * is called with argument "-g"
    */
    GenerationsArgument() : BaseArgument("-g") {}
    /**
    * @brief The destructor of class
    */
    ~GenerationsArgument() {}
    /**
    * @brief Executes the generations argument.
    * @details If a value is provided the maxGenerations member of 
    * the appValues argument is set by parsing the string value to
    * an int by using the stoi function.
    * @param appValues A reference to ApplicationValues.
    * @param generations A null terminated char array containing the value of argument.
    */
    void execute(ApplicationValues& appValues, char* generations);
};
/**
* @class WorldsizeArgument
* @brief Implements the world size argument.
* @details The world size argument controls the width and
* height of the world. This class handles the execution of
* this argument.
*/
class WorldsizeArgument : public BaseArgument {
public:
    /**
    * @brief the constructor of class
    * @details To construct the object, the constructor of base class
    * is called with argument "-s"
    */
    WorldsizeArgument() : BaseArgument("-s") {}
    /**
    * @brief The destructor of class
    */
    ~WorldsizeArgument() {}
    /**
    * @brief Executes the world size argument.
    * @details If a value is provided the width and height
    * of the world is read from the provided value. Otherwise
    * it shows an error message.
    * @param appValues A reference to ApplicationValues.
    * @param dimensions A null terminated char array containing the value of argument.
    */
    void execute(ApplicationValues& appValues, char* dimensions);
};
/**
* @class FileArgument
* @brief Implements the file argument.
* @details The file argument is used to specify a path
* for the file to read initial state form.
*/
class FileArgument : public BaseArgument {
public:
    /**
    * @brief the constructor of class
    * @details To construct the object, the constructor of base class
    * is called with argument "-f"
    */
    FileArgument() : BaseArgument("-f") {}
    /**
    * @brief The destructor of class
    */
    ~FileArgument() {}
    /**
    * @brief Executes the file argument.
    * @details If a value is provided the global variable 'fileName'
    * is set to the value. Otherwise it shows an error message.
    * @param appValues A reference to ApplicationValues.
    * @param fileNameArg A null terminated char array containing the value of argument.
    */
    void execute(ApplicationValues& appValues, char* fileNameArg);
};

/**
* @class EvenRuleArgument
* @brief Implements the even rule argument.
* @details The even rule argument is used to specify the name
* if the rule used in number even generations.
*/

class EvenRuleArgument : public BaseArgument {
public:
    /**
    * @brief the constructor of class
    * @details To construct the object, the constructor of base class
    * is called with argument "-er"
    */
    EvenRuleArgument() : BaseArgument("-er") {}
    /**
    * @brief The destructor of class
    */
    ~EvenRuleArgument() {}
    /**
    * @brief Executes the even rule argument.
    * @details If a value is provided, the evenRuleName member
    * of appValues is set to the value of the argument. Otherwise
    * it shows an error message.
    * @param appValues A reference to ApplicationValues.
    * @param evenRule A null terminated char array containing the value of argument.
    */
    void execute(ApplicationValues& appValues, char* evenRule);
};

/**
* @class OddRuleArgument
* @brief Implements the odd rule argument.
* @details The odd rule argument is used to specify the name
* if the rule used in number odd generations.
*/
class OddRuleArgument : public BaseArgument {
public:
    /**
    * @brief the constructor of class
    * @details To construct the object, the constructor of base class
    * is called with argument "-or"
    */
    OddRuleArgument() : BaseArgument("-or") {}
    /**
    * @brief The destructor of class
    */
    ~OddRuleArgument() {}
    /**
    * @brief Executes the odd rule argument.
    * @details If a value is provided, the oddRuleName member
    * of appValues is set to the value of the argument. Otherwise
    * it shows an error message.
    * @param appValues A reference to ApplicationValues.
    * @param oddRule A null terminated char array containing the value of argument.
    */
    void execute(ApplicationValues& appValues, char* oddRule);
};

#endif //GAMEOFLIFE_MAINARGUMENTS_H
