/*
 * @file    Globals.h
 * @author      Erik Ström
 * @date        October 2017
 * @version     0.1
 * @brief contains the globals used by program.
*/

#ifndef GLOBALS_H
#define GLOBALS_H

#include <string>
#include "SupportStructures.h"

using namespace std;

// The actual width and height of the used world
extern Dimensions WORLD_DIMENSIONS;

// Name of file to read
extern string fileName;


#endif