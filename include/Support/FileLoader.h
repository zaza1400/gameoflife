/**
 * @file    FileLoader.h
 * @author      Erik Ström
 * @date        October 2017
 * @version     0.1
*/

#ifndef FileLoaderH
#define FileLoaderH

#include <map>
#include "Cell_Culture/Cell.h"
#include "Globals.h"

using namespace std;

/**
* @class FileLoader
* @brief Loads the starting state for the simulation, based on contents of specified file.
* @details Reads startup values from specified file, containing values for WORLD_DIMENSIONS
* and cell Population. Will create the corresponding cells.
*/
class FileLoader {

public:
	/**
	* @brief The constructor of FileLoader class;
	*/
    FileLoader() {}


    /**
    * @brief Loads the given map with cells read from a file
    * @details data is read from a file that is pointed to by
    * The global variable fileName. It first reads the world 
    * dimensions from the file. then iterate through all
   	* positions to create corresponding cell to the information
   	* red from the file.
   	* @param cells A reference to map of cells to be filled with the data read form file.
    * @throw ios_base::failure
    * @test if throws exception on invalid file
    * @test if loads file if path is valid
    * @test if the loaded world size is currect
    * @test if the number of loaded cells is correct
	* @test if the number of loaded alive cells is correct
	* @test if boundry cells are rim cells
    * @bug The size of population is not currect
    * @bug does not load the population correct
    */
    void loadPopulationFromFile(map<Point, Cell>& cells);

};

#endif
