/**
 * @file    MainArgumentsParser.h
 * @author      Erik Ström
 * @date        October 2017
 * @version     0.1
*/

#ifndef mainArgumentsParserH
#define mainArgumentsParserH

#include "MainArguments.h"
#include <string>
#include <algorithm>
#include <sstream>

using namespace std;

/**
* @class MainArgumentsParser
* @brief It parses arguments of the application
* @details The derived classes of BaseArgument are used to
* execute arguments.
*/
class MainArgumentsParser {
public:
	/**
	* @brief parses the arguments, executes them and return the ApplicationValues
	* @details Parses the arguments and creates objects of classes that are derived
	* from the BaseArgument class. These object are used to execute the argument and
	* modify the returned ApplicationValues
	* @param argv a array of arguments. Each argument is a null-terminated char array.
	* @param length Number of arguments
	* @bug if the argument is -h runtime error 'use of uninitialized varibale' will happen
	* @test check if it works for all argument types
	*/
    ApplicationValues& runParser(char* argv[], int length);

private:
    ApplicationValues appValues;

    // Checks if a given option exists
    bool optionExists(char** begin, char** end, const std::string& option);

    // gets the given option value
    char* getOption(char** begin, char** end, const std::string& option);
};

#endif
