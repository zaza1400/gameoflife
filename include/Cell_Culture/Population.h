/**
 * @file    
 * @author      Erik Ström
 * @date        October 2017
 * @version     0.1
*/

#ifndef POPULATION_H
#define POPULATION_H

#include<map>
#include<string>
#include "Cell.h"
#include "Support/Globals.h"
#include "GoL_Rules/RuleOfExistence.h"
#include "GoL_Rules/RuleFactory.h"

using namespace std;

/**
* @class Population
* @brief Representation of the complete society of cell existence and interactions.
* @details The Population constitutes of all current, previous and future generations of cells, both living and dead
* as well as those not yet born. By mapping all cells to their respective positions in the simulation world,
* Population has the complete knowledge of each cell's whereabouts. Furthermore, the class is responsible for
* determining which rules should be required from the RuleFactory, and store the pointer to these as members.
* Population's main responsibility during execution is determining which rule to apply for each new generation
* and updating the cells to their new states.
*/
class Population
{
private:
    int generation;
    map<Point, Cell> cells;
    RuleOfExistence* evenRuleOfExistence;
    RuleOfExistence* oddRuleOfExistence;

    void randomizeCellCulture();
    void buildCellCultureFromFile();

public:
    /**
    * @brief The constructor of the Population class
    * @details It sets generation to zero and odd and even rules to nullptr
    * @test if rules are set to nullprt
    */
    Population() : generation(0), evenRuleOfExistence(nullptr), oddRuleOfExistence(nullptr) {}

    /**
    * @brief Destructor of the Population class that frees allocated memory.
    * @details It deletes the evenRuleOfExistence and oddRuleOfExistence pointers
    */
    ~Population();

    /**
    * @brief Builds cell culture based on randomized starting values.
    * @details It allocates and maps cells based on world Size
    * and randomly choose alive or dead cells based on uniform distribution
    * @param evenRuleName a string to specify name of the rule used in even generations
    * @param oddRuleName a string to specify name of the rule used in odd generations.
    * This parameter is optional and have the default value of "".
    * @bug the size of the generated population does not match world size
    * @test check if parameters are invalid, the rules are default as Conway's
    * @test check if not passing oddRuleName will cause the both rules to be same
    * @test if when filename is empty it correclty generate a random population
    * @test when filename is invalid exception thorwn
    * @test when filename is valid, populaiton is loaded
    */
    void initiatePopulation(string evenRuleName, string oddRuleName = "");

    /**
    * @brief calculates the next generation.
    * @details Update the cell population and determine next generational changes based on rules. 
    * @return int Number of generations so far.
    * @test if generation number is advanced
    */
    int calculateNewGeneration();

    /**
    * @brief to get a cell by knowing its position
    * @details uses the input parameter as a key to look in the cells map
    * @return Cell& The cell at the specified position.
    */
    Cell& getCellAtPosition(Point position) { return cells.at(position); }

    /**
    * @brief Gets the number of cells
    * @details Gets the total amount of cells in the population, regardless of state.
    * @return int The total number of cells regardless of state.
    */
    int getTotalCellPopulation() { return cells.size(); }

    /**
    * @brief For use in unit tests
    */
    RuleOfExistence* getEvenRule() { return evenRuleOfExistence; }
    /**
    * @brief For use in unit tests
    */
    RuleOfExistence* getOddRule() { return oddRuleOfExistence; }

};

#endif