/**
 * @file    
 * @author      Erik Ström
 * @date        October 2017
 * @version     0.1
 * @brief Contains the definition for 
*/

#ifndef cellH
#define cellH

#include "../../terminal/terminal.h"

/**
* @struct StateColors
* @brief Data structure holding colors to visualize the state of cells.
* @details This struct holds four members of type COLOR, one for each
* state of cell. The states are LIVING,DEAD,OLD,ELDER
*/
struct StateColors {
    COLOR LIVING, // Representing living cell
            DEAD, // Representing dead cell
            OLD,  // Representing old cell
            ELDER;// Representing very old cell
}	/* Initiate default values. */
const STATE_COLORS = { COLOR::WHITE, COLOR::BLACK, COLOR::CYAN, COLOR::MAGENTA };

/**
* @enum Action
* @brief Possible actions for each cell
* @details Actions Determined by the rule, and sent to cell for future change.
*/
enum ACTION { KILL_CELL, IGNORE_CELL, GIVE_CELL_LIFE, DO_NOTHING };

/**
* @class Cell
* @brief Cells represents a certain combination of row and column of the simulated world.
* @details Cells may be of two types; rim cells, those representing the outer limits of the world,
* or non-rim cells. The first cell type are immutable, exempt from the game's rules, and
* thus their values may not be changed. The latter type, however, may be changed and edited
* in ways specified by the rules.
*/
class Cell {

private:
    struct CellDetails {	// encapsulate cell details
        int age;
        COLOR color;
        bool rimCell;
        char value;
    } details;

    struct NextUpdate {		// encapsulate changes to next state
        ACTION nextGenerationAction;
        COLOR nextColor;
        char nextValue;
        bool willBeAlive;	// some rules may need to know beforehand whether the cell will be alive
    } nextUpdate;


    void incrementAge() { details.age++; }

    void killCell() { details.age = 0; }

    // Sets the character value of the cell, which will be printed to screen.
    void setCellValue(char value) { details.value = value; }

    void setColor(COLOR color) { this->details.color = color; }

public:
    /**
    * @brief The constructor of the Cell class.
    * @details determines the cell's starting values.
    * The cell will update to its initial state
    * @param isRimCell A bool value that specifies if cell is a rim cell.
    * @param action The action for this cell in the next generation.
    * @test if isRimCell parameter works
    * @test if rim cells are dead and their age is 0
    * @test if the value of cell is '#'
    * @test if actions are applied correctly
    */
    Cell(bool isRimCell = false, ACTION action = DO_NOTHING);

    /**
    * @brief Is the cell Alive?
    * @details For rim cells it always returns false.
    * For normal cells it returns true if age>0 and
    * and false otherwise.
    * @test if returns false for rim cells
    * @test if returns true for ordinary cells whose age > 0
    */
    bool isAlive();

    /**
    * @brief Sets the cells next action to take in its coming update.
    * @details It does nothing if the cell is a rim cell or the cell
    * action is GIVE_CELL_LIFE and cell is already alive. Otherwise
    * it stores the action as the next generation action.
    * @param action A Value of type ACTION indicating the next action.
    * @test if next action is stored for ordinary cells
    * @test if next action is not set if cell is rim cell
    */
    void setNextGenerationAction(ACTION action);

    /**
    * @brief Updates the cell to its new state, based on stored update values.
    * @details It applies the next generation action to the cell, sets the color
    * and value of the cell after update and resets the next generation action.
    * @test if it resets next action
    * @test if it applies the next color and next value
    * @test if it applies actions currectly (test covers all branches)
    */
    void updateState();

    /**
    * @brief returns the age of the cell.
    * @return int The age of the cell
    * @test age is returned correclty
    * @test age is increamented correctly
    */
    int getAge() { return details.age; }

    /**
    * @brief returns the color of the cell.
    * @return COLOR The color of the cell
    * @test if the correct color is returned
    */
    COLOR getColor() { return details.color; }

    /**
    * @brief Determines whether the cell is a rim cell, and thus should be immutable
    * @return bool If the cell is a rim cell returns true, otherwise false.
    * @test if correct value is returned
    */
    bool isRimCell() { return details.rimCell; }

    /**
    * @brief Sets the color the cell will have after its next update.
    * @param nextColor An instance of COLOR to specify the next color of the cell.
    * @test checking if the next color is set
    */
    void setNextColor(COLOR nextColor) { this->nextUpdate.nextColor = nextColor; }

    /**
    * @brief Returns value of the cell.
    * @return char The value of the cell on the sreen.
    * @test if the correct value is returned
    */
    char getCellValue() { return details.value; }

    /**
    * @brief Sets the next character value of the cell, which will be printed to screen.
    * @param char a char value which will be used to print cell on the screen
    * @test if next value is set
    */
    void setNextCellValue(char value) { nextUpdate.nextValue = value; }

    /**
    * @brief Sets whether the cell is alive/dead next generation.
    * @param isAliveNext a bool value indicating if the will be alive
    * in the next generation.
    */
    void setIsAliveNext(bool isAliveNext) { nextUpdate.willBeAlive = isAliveNext; }

    /**
    * @brief Will the cell be alive next generation?
    * @return bool true if the node is alive, false otherwise
    */
    bool isAliveNext() { return nextUpdate.willBeAlive; }

    /** 
    * @brief Gets the cells next action.
    * @return ACTION& the next generation action.
    * @bug the willBeAliveNext is not set
    * @test if correct value is returned
    */
    ACTION& getNextGenerationAction() { return nextUpdate.nextGenerationAction; }

};

#endif
