/**
 * @file
 * @author      Erik Ström
 * @date        October 2017
 * @version     0.1
 * @brief contains definition for the ScreenPrinter class
*/



#ifndef screenPrinterH
#define screenPrinterH


#include "../terminal/terminal.h"
#include "Cell_Culture/Population.h"
/**
* @class ScreenPrinter
* @brief Responsible for visually representing the simulation world on screen.
* @details This class is implemented as a singleton. It is used to print
* various types of information such as the population on the terminal.
* 
*/

class ScreenPrinter {

private:
    Terminal terminal;

    ScreenPrinter() {}

public:
    /**
    * @brief This method implements the singleton pattern.
    * @details The instance is created upon first use of the function
    * and is returned as a reference.
    */
    static ScreenPrinter& getInstance() {
        static ScreenPrinter instance;
        return instance;
    }

    /**
    * @brief Prints the population to screen
    * @details It iterates through all rows and columns of the
    * population and prints each individual  cell as a character
    * with suitable color
    * @param population a reference to the population to be printed.
    */
    void printBoard(Population& population);

    /**
    * @brief Prints the help screen
    * @details The help screen contains information on how to use
    * the command life interface and applicable  command life arguments.
    */
    void printHelpScreen();

    /**
    * @brief Prints a message to show some information to the user.
    * @details the message passed as the parameter  will be printed
    * followed by a new line character (std::endl)
    * @param message The message to be printed.
    */
    void printMessage(string message);

    /**
    * @brief Clears the terminal
    * @details clears the terminal by calling the clear method
    * on the terminal object
    */
    void clearScreen();
};

#endif
