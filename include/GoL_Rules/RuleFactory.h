/**
 * @file    RuleFactory.h
 * @author      Erik Ström
 * @date        October 2017
 * @version     0.1
*/

#ifndef RULEFACTORY_H
#define RULEFACTORY_H

#include "GoL_Rules/RuleOfExistence.h"

/**
* @brief Singleton class to handle creation of RulesOfExistence objects.
* @details This is a factory class to make RulesOfExistence objects.
* It is implemented as a singleton
*/

class RuleFactory
{
private:
    RuleFactory() {}

public:
	/**
	* @brief Singleton factory receiver.
	* @details The instance is created first time this method is called
	* @return RuleFactory& The single instance
	*/
    static RuleFactory& getInstance();

    /**
    * @brief Creates and returns specified RuleOfExistence.
    * @details This method defaults to Conway's rule
    * @return RuleOfExistence* the created object
    * @param cells A reference to the map of cells which is passed to
    * the constructor of the rule object.
    * @param ruleName A string value to indicate what type of rule should
    * be created. the default value is "conway"
    * @test by passing invalid rule name a default rule (conway) is returned
    * @test by passing no rule a default rule (conway) is returned
    * @test by passing name of rule, the correct rule is returned
    */
    RuleOfExistence* createAndReturnRule(map<Point, Cell>& cells, string ruleName = "conway");
};

#endif