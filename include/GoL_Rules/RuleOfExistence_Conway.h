/**
 * @file    RuleOfExistence_Conway.h
 * @author      Erik Ström
 * @date        October 2017
 * @version     0.1
 * @brief Contains the definition for RuleOfExistence_Conway class
*/

#ifndef GAMEOFLIFE_RULEOFEXISTENCE_CONWAY_H
#define GAMEOFLIFE_RULEOFEXISTENCE_CONWAY_H

#include "RuleOfExistence.h"

/**
* @class RuleOfExistence_Conway
* @brief Conway's RuleOfExistence, applying actions based on PopulationLimits on all 8 surrounding neighbours.
* @details Concrete RuleOfExistence, implementing Conway's rule of determining alive neighbours surrounding the cell
* by checking all 8 directions, 4 x Cardinal + 4 x Diagonal. PopulationLimits are set as;
* UNDERPOPULATION<2	 => Cell dies of loneliness
* OVERPOPULATION>3 => Cell dies of overcrowding**
* RESURRECTION=3 =>Cell is infused with life**
*/
class RuleOfExistence_Conway : public RuleOfExistence
{
private:

public:
	/**
	* @brief The constructor of RuleOfExistence_Conway class
	* @details it passes the paramters to the parent class
	* (RuleOfExistence) constructor. it specifies population limits,
	* direction and rule name and passes through the received cells argument
	* @param cells a reference to the map of cells
    * @test if correct values are stored in object after construction
	*/
    RuleOfExistence_Conway(map<Point, Cell>& cells)
            : RuleOfExistence({ 2,3,3 }, cells, ALL_DIRECTIONS, "conway") {}
    /**
    * @brief Destructor of the class
    */
    ~RuleOfExistence_Conway() {}
    /**
    * @brief Executes the rule specific for Conway.
    * @details calculates how many neighbouring cells are alive,
    * and decides about the next generation action and color by
    * applying the Conway's rules.
    * @test the execution of rule on the population is correct
    */
    void executeRule();
};

#endif //GAMEOFLIFE_RULEOFEXISTENCE_CONWAY_H
