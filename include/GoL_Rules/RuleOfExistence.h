/**
 * @file    RuleOfExistence.h
 * @author      Erik Ström
 * @date        October 2017
 * @version     0.1
 * @details These rules lies at the heart of the simulation,
 * and determines the fate of each cell in the world population.
*/
#ifndef RULEOFEXISTENCE_H
#define RULEOFEXISTENCE_H

#include<string>
#include<map>
#include<vector>
#include "Cell_Culture/Cell.h"
#include "Support/Globals.h"
using namespace std;


/**
* @brief Data structure for storing population limits. Used by rules to determine what ACTION to make.
*/
struct PopulationLimits{
    int UNDERPOPULATION, // cell dies of loneliness
            OVERPOPULATION, // cell dies of over crowding
            RESURRECTION; // cell lives on / is resurrected
};

/**
* @brief Data structure for storing directional values. Used by rules to determine neighboring cells.
*/
struct Directions {
    int HORIZONTAL, VERTICAL;
};

/**
* @brief All directions; N, E, S, W, NE, SE, SW, NW
*/
const vector<Directions> ALL_DIRECTIONS{ { 0,-1 },{ 1,0 },{ 0,1 },{ -1,0 },{ 1,-1 },{ 1,1 },{ -1,1 },{ -1,-1 } };

/**
* @brief Cardinal directions; N, E, S, W
*/
const vector<Directions> CARDINAL{ { 0,-1 },{ 1,0 },{ 0,1 },{ -1,0 } };

/**
* @brief Diagonal directions; NE, SE, SW, NW
*/
const vector<Directions> DIAGONAL{ { 1,-1 },{ 1,1 },{ -1,1 },{ -1,-1 } };

/**
* @class RuleOfExistence
* @brief Abstract base class, upon which concrete rules will derive.
* @details The derivations of RuleOfExistence is what determines the culture of Cell Population. Each rule implements
* specific behaviours and so may execute some parts in different orders. In order to accommodate this
* requirement RuleOfExistence will utilize a **Template Method** design pattern, where all derived rules
* implements their logic based on the virtual method executeRule().
*/
class RuleOfExistence {
protected:
    /**
    * @brief The name of the rule
    */
    string ruleName;

    /**
    * @brief Reference to the population of cells
    */
    map<Point, Cell>& cells;

    /**
    * @brief Amounts of alive neighboring cells, with specified limits
    */
    const PopulationLimits POPULATION_LIMITS;

    /**
    * @brief The directions, by which neighboring cells are identified
    */
    const vector<Directions>& DIRECTIONS;

    /**
    * @brief counts the number of alive neighbors
    * @details Determines the amount of alive neighboring cells to current cell,
    * using directions specified by the rule. Checks neighboring cells in all 
    * directions relevant for the rule.
    * @param currentPoint A Point specifying which cell to count neighbors of.
    * @return int The number of alive neighbors.
    */
    int countAliveNeighbours(Point currentPoint);
    /**
    * @brief Determines what action should be taken regarding the current cell, based on alive neighboring cells.
    * @details It ignores, kills or resurrect cells based on the population limits
    * @param aliveNeighbours an int value indicating the number of alive neighbors
    * @param isAlive a bool value indicating if the cell is alive
    * @return ACTION The suitable action.
    */
    ACTION getAction(int aliveNeighbours, bool isAlive);

public:
    /**
    * @brief The constructor for RuleOfExistence class.
    * @details the parameters are used to initialize population limits, cells map, directions
    * and the rule name.
    * @param limits an object of type PopulationLimits to specify population limits
    * @param cells A reference to the map of cells
    * @param DIRECTIONS The directions that are considered neighbors
    * @param ruleName A string value to specify name of the rule.
    */
    RuleOfExistence(PopulationLimits limits, map<Point, Cell>& cells, const vector<Directions>& DIRECTIONS, string ruleName)
            : POPULATION_LIMITS(limits), cells(cells), DIRECTIONS(DIRECTIONS), ruleName(ruleName) {}
    /**
    * @brief the destructor of RuleOfExistence class
    * @details It does nothing. It is defined as virtual
    * to let objects of derived classes be destructed by
    * deleting a pointer of the base class (this class).
    */
    virtual ~RuleOfExistence() {}

    /**
    * @brief Execute rule, in order specific to the concrete rule, by utilizing template method DP
    * @details This is a purely virtual method. Each concrete derived class should
    * implements its own code for this method.
    */
    virtual void executeRule() = 0;

    /**
    * @brief Gets the name of the rule
    * @return string The name of the rule.
    */
    string getRuleName() { return ruleName; }

	/**
	* @brief used in unit tests
	*/
	const PopulationLimits& getPopulationLimits(){return POPULATION_LIMITS;}
	
	/**
	* @brief used in unit tests
	*/
	int getDirectionsCount(){ return DIRECTIONS.size(); };
};

#endif