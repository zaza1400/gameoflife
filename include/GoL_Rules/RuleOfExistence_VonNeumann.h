/**
 * @file    RuleOfExistence_VonNeumann.h
 * @author      Erik Ström
 * @date        October 2017
 * @version     0.1
 * @brief Contains the definition for RuleOfExistence_VonNeumann class
*/

#ifndef GAMEOFLIFE_RULEOFEXISTENCE_VONNEUMANN_H
#define GAMEOFLIFE_RULEOFEXISTENCE_VONNEUMANN_H

#include "RuleOfExistence.h"


/**
* @class RuleOfExistence_VonNeumann
* @brief Von Neumann's RuleOfExistence, differs from Conway in that only 4 neighbours are accounted for.
* @details Concrete Rule of existence, implementing Von Neumann's rule.
* Only difference from Conway is that neighbours are determined using only cardinal directions (N, E, S, W).
*/
class RuleOfExistence_VonNeumann : public RuleOfExistence
{
private:

public:
	/**
	* @brief The constructor of RuleOfExistence_Conway class
	* @details it passes the paramters to the parent class
	* (RuleOfExistence) constructor. it specifies population limits,
	* direction (only cardinal directions) and rule name and passes
	* through the received cells argument.
	* @param cells a reference to the map of cells
    * @test if correct values are stored in object after construction
	*/
    RuleOfExistence_VonNeumann(map<Point, Cell>& cells)
            : RuleOfExistence({ 2,3,3 }, cells, CARDINAL, "von_neumann") {}
    /**
    * @brief Destructor of the class
    */
    ~RuleOfExistence_VonNeumann() {}
	/**
    * @brief Execute the rule specific for Von Neumann.
    * @details calculates how many neighbouring cells are alive,
    * and decides about the next generation action and color by
    * applying the Von Neumann's rules.
    * @test the execution of rule on the population is correct
    */
    void executeRule();
};

#endif //GAMEOFLIFE_RULEOFEXISTENCE_VONNEUMANN_H
