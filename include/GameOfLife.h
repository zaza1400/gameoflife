/**
 * @file 		
 * @author		Erik Ström
 * @date        October 2017
 * @version     0.1
 * @brief		contains declaration of class Game of life
*/

#ifndef GameOfLifeH
#define GameOfLifeH

#include "Cell_Culture/Population.h"
#include "ScreenPrinter.h"
#include <string>

/**
* @class GameOfLife
* @brief The heart of the simulation, interconnects the main execution with the graphical presentation.
* @details Creates and manages Population and the ScreenPrinter instances. Will instruct the Population of cells to keep
* updating as long as the specified number of generations is not reached, and for each iteration instruct
* ScreenPrinter to write the information on screen.
*/
class GameOfLife {

private:
    Population population;
    ScreenPrinter& screenPrinter;
    int nrOfGenerations;

public:
	/**
	* @brief Constructor of the GameOfLife class
	* @details The first parameter is number of generations and two string parameters
	* are names for even and odd generation number rules. the screenPrinter instance 
	* created.
	* @param nrOfGenerations An int value to specify number of generatiosn
	* @param evenRuleName A string value to specify rule name for even generations
	* @param oddRuleName A string value to specify rule name for odd generations
	*/
    GameOfLife(int nrOfGenerations, string evenRuleName, string oddRuleName);

   	/**
   	* @brief Runs the simulation for as many generations as been set by the user (default = 500).
   	* @details It starts by clearing the terminal and printing the initial population
   	* then proceeds to do the following actions in each iteration:
   	* 1- Calculating the new generation
   	* 2- Pring the new genration
   	* 3- Sleep for 100 milliseconds
    * @test if it thorws expcetion if invalid parameteres are passe to the constructor
    * @bug does not throw expcetion if invalid parameteres are passe to the constructor
   	*/
    void runSimulation();

};

#endif
