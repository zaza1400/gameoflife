# Game of life
This is a fork of the GameOfLife project. This repository adds documentation and testing to the existing Game Of Life code.

The documentation is generated using doxygen. The doxygen files are in "doc" folder. To generate the html pages, open command line in the "doc" directory and run the following command:
```
doxygen
```
The source file for unit tests can be found in the "test" dicrectory.
## Requirements

To build the documentation, doxygen must be installed. CMake is also required to to build and run both the project itself and the unit tests.

zaza1400@student.miun.se
