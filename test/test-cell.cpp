/**
* file        test-cell.cpp
* author      zaza1400
* date        November 2018
* version     1.0
**/

#include <string>
#include "catch.hpp"
#include "Cell_Culture/Cell.h"

SCENARIO("Test constructor of the cell class")
{
	GIVEN("An ordinary cell")
	{
		Cell c(false);
		THEN("The cell should not be a rim cell")
		{
			REQUIRE_FALSE(c.isRimCell());
		}
		THEN("The cell should be dead")
		{
			REQUIRE_FALSE(c.isAlive());
			REQUIRE((c.getColor() == STATE_COLORS.DEAD));
		}
		THEN("The age should be 0")
		{
			REQUIRE(c.getAge() == 0);
		}
		THEN("Value of the cell should be #")
		{
			REQUIRE(c.getCellValue() == '#');
		}
		THEN("The next generation action should be reseted to DO_NOTHING")
		{
			REQUIRE(c.getNextGenerationAction() == DO_NOTHING);
		}
	}
	GIVEN("An ordinary cell with GIVE_CELL_LIFE action")
	{
		Cell c(false, ACTION::GIVE_CELL_LIFE);
		THEN("The cell should not be a rim cell")
		{
			REQUIRE_FALSE(c.isRimCell());
		}
		THEN("The cell should be alive")
		{
			REQUIRE(c.isAlive());
			REQUIRE((c.getColor() == STATE_COLORS.LIVING));
		}
		THEN("The age should be 1")
		{
			REQUIRE(c.getAge() == 1);
		}
		THEN("The next generation action should be reseted to DO_NOTHING")
		{
			REQUIRE(c.getNextGenerationAction() == DO_NOTHING);
		}
	}
	GIVEN("An ordinary cell with IGNORE_CELL action")
	{
		Cell c(false, ACTION::IGNORE_CELL);
		THEN("The cell should not be a rim cell")
		{
			REQUIRE_FALSE(c.isRimCell());
		}
		THEN("The cell should be dead")
		{
			REQUIRE_FALSE(c.isAlive());
			REQUIRE((c.getColor() == STATE_COLORS.DEAD));
		}
		THEN("The age should be 0")
		{
			REQUIRE(c.getAge() == 0);
		}
		THEN("Value of the cell should be #")
		{
			REQUIRE(c.getCellValue() == '#');
		}
		THEN("The next generation action should be reseted to DO_NOTHING")
		{
			REQUIRE(c.getNextGenerationAction() == DO_NOTHING);
		}
	}
	GIVEN("An ordinary cell with KILL_CELL action")
	{
		Cell c(false, ACTION::KILL_CELL);
		THEN("The cell should not be a rim cell")
		{
			REQUIRE_FALSE(c.isRimCell());
		}
		THEN("The cell should be dead")
		{
			REQUIRE_FALSE(c.isAlive());
			REQUIRE((c.getColor() == STATE_COLORS.DEAD));
		}
		THEN("The age should be 0")
		{
			REQUIRE(c.getAge() == 0);
		}
		THEN("Value of the cell should be #")
		{
			REQUIRE(c.getCellValue() == '#');
		}
		THEN("The next generation action should be reseted to DO_NOTHING")
		{
			REQUIRE(c.getNextGenerationAction() == DO_NOTHING);
		}
	}
	GIVEN("A rim cell")
	{
		Cell c(true);
		THEN("The cell should be a rim cell")
		{
			REQUIRE(c.isRimCell());
		}
		THEN("The cell should be dead")
		{
			REQUIRE_FALSE(c.isAlive());
			REQUIRE((c.getColor() == STATE_COLORS.DEAD));
		}
		THEN("The age should be 0")
		{
			REQUIRE(c.getAge() == 0);
		}
		THEN("The next generation action should be reseted to DO_NOTHING")
		{
			REQUIRE(c.getNextGenerationAction() == DO_NOTHING);
		}
	}
}
SCENARIO("Test getAge and isAlive methods")
{
	GIVEN("A rim cell ")
	{
		Cell c(true);
		THEN("getAge should return 0")
		{
			REQUIRE(c.getAge() == 0);
		}
		THEN("isAlive should return false")
		{
			REQUIRE_FALSE(c.isAlive());
		}
	}
	GIVEN("an ordinary cell")
	{
		Cell c(false, ACTION::GIVE_CELL_LIFE);
		THEN("get age should return 1")
		{
			REQUIRE(c.getAge() == 1);
		}
		THEN("isAlive should return true")
		{
			REQUIRE(c.isAlive());
		}
		WHEN("The cell is updated 10 times")
		{
			for (int i = 0; i < 10; i++)
			{
				c.setNextGenerationAction(ACTION::IGNORE_CELL);
				c.updateState();
			}
			THEN("The age should be 11")
			{
				REQUIRE(c.getAge() == 11);
			}
		}
		
	}
}
SCENARIO("Test is setIsAliveNext and isAliveNext methods")
{
	GIVEN("A rim cell")
	{
		Cell c(true);
		WHEN("the setIsAliveNext is called with true")
		{
			c.setIsAliveNext(true);
			THEN("isAliveNext should return true")
			{
				REQUIRE(c.isAliveNext());
			}
		}
		WHEN("the setIsAliveNext is called with false")
		{
			c.setIsAliveNext(false);
			THEN("isAliveNext should return false")
			{
				REQUIRE_FALSE(c.isAliveNext());
			}
		}
	}
	GIVEN("An ordinary cell")
	{
		Cell c(false);
		WHEN("the setIsAliveNext is called with true")
		{
			c.setIsAliveNext(true);
			THEN("isAliveNext should return true")
			{
				REQUIRE(c.isAliveNext());
			}
		}
		WHEN("the setIsAliveNext is called with false")
		{
			c.setIsAliveNext(false);
			THEN("isAliveNext should return false")
			{
				REQUIRE_FALSE(c.isAliveNext());
			}
		}
	}
}
SCENARIO("Test getCellValue and setNextCellValue method")
{
	GIVEN("An ordinary cell")
	{
		Cell c(false);
		THEN("getCellValue should return #")
		{
			REQUIRE(c.getCellValue() == '#');
		}
		WHEN("Setting next cell value to '!' and calling updateState method")
		{
			c.setNextCellValue('!');
			c.updateState();
			THEN("getCellValue should return !")
			{
				REQUIRE(c.getCellValue() == '!');
			}
		}
	}
}
SCENARIO("Test getColor and SetNextColor methods")
{
	GIVEN("An alive ordinary cell")
	{
		Cell c(false, ACTION::GIVE_CELL_LIFE);
		THEN("getColor should return  STATE_COLORS.LIVING")
		{
			REQUIRE((c.getColor() == STATE_COLORS.LIVING));
		}
		WHEN("Setting next cell color to STATE_COLORS.OLD and calling updateState method")
		{
			c.setNextColor(STATE_COLORS.OLD);
			c.updateState();
			THEN("getColor should return STATE_COLORS.OLD")
			{
				REQUIRE((c.getColor() == STATE_COLORS.OLD));
			}
		}
	}
}
SCENARIO("Test getNextGenerationAction and setNextGenerationAction")
{
	GIVEN("A cell")
	{
		Cell c;
		THEN("getNextGenerationAction should return DO_NOTHING")
		{
			REQUIRE(c.getNextGenerationAction() == ACTION::DO_NOTHING);
		}
		WHEN("Calling setNextGenerationAction with IGNORE_CELL")
		{
			c.setNextGenerationAction(IGNORE_CELL);
			THEN("getNextGenerationAction should return IGNORE_CELL")
			{
				REQUIRE(c.getNextGenerationAction() == ACTION::IGNORE_CELL);
			}
		}
	}
	GIVEN("A rim cell")
	{
		Cell c(true);
		WHEN("Calling setNextGenerationAction with IGNORE_CELL")
		{
			c.setNextGenerationAction(IGNORE_CELL);
			THEN("getNextGenerationAction should return DO_NOTHING")
			{
				REQUIRE(c.getNextGenerationAction() == ACTION::DO_NOTHING);
			}
		}
	}
}
SCENARIO("Test isRimCell")
{
	GIVEN("A rim cell")
	{
		Cell c(true);
		THEN("isRimCell should return true")
		{
			REQUIRE(c.isRimCell());
		}
	}
	GIVEN("An ordinary cell")
	{
		Cell c(false);
		THEN("isRimCell should return false")
		{
			REQUIRE_FALSE(c.isRimCell());
		}
	}
}
SCENARIO("Test updateState")
{
	GIVEN("A rim cell")
	{
		Cell c(true);
		WHEN("Setting the next action to GIVE_CELL_LIFE and calling updateState")
		{
			c.setNextGenerationAction(ACTION::GIVE_CELL_LIFE);
			THEN("cell should be dead")
			{
				REQUIRE_FALSE(c.isAlive());
			}
		}
	}
	GIVEN("An ordinary cell")
	{
		Cell c(false);
		WHEN("Calling updateState")
		{
			c.updateState();
			THEN("The next action should be reseted to DO_NOTHING")
			{
				REQUIRE(c.getNextGenerationAction() == ACTION::DO_NOTHING);
			}
		}
		WHEN("Setting the next color to COLOR::BLUE and calling updateState")
		{
			c.setNextColor(COLOR::BLUE);
			c.updateState();
			THEN("getColor should return COLOR::BLUE")
			{
				REQUIRE((c.getColor() == COLOR::BLUE));
			}
		}
		WHEN("Setting the next value to '@' and calling updateState")
		{
			c.setNextCellValue('@');
			c.updateState();
			THEN("getCellValue should return '@'")
			{
				REQUIRE(c.getCellValue() == '@');
			}
		}
		
	}
	GIVEN("An alive ordinary cell")
	{
		Cell c(false, ACTION::GIVE_CELL_LIFE);
		WHEN("Setting the next action to IGNORE_CELL and calling updateState")
		{
			c.setNextGenerationAction(ACTION::IGNORE_CELL);
			c.updateState();
			THEN("getAge should return 2")
			{
				REQUIRE(c.getAge() == 2);
			}
		}
		WHEN("Setting the next action to GIVE_CELL_LIFE and calling updateState")
		{
			c.setNextGenerationAction(ACTION::GIVE_CELL_LIFE);
			c.updateState();
			THEN("getAge should return 1")
			{
				REQUIRE(c.getAge() == 1);
			}
		}
		WHEN("Setting the next action to KILL_CELL and calling updateState")
		{
			c.setNextGenerationAction(ACTION::KILL_CELL);
			c.updateState();
			THEN("Cell should be dead")
			{
				REQUIRE_FALSE(c.isAlive());
				REQUIRE(c.getAge() == 0);
			}
		}
	}
}