/**
* file        test-cell.cpp
* author      zaza1400
* date        November 2018
* version     1.0
**/

#include <string>
#include "catch.hpp"
#include "Cell_Culture/Population.h"


SCENARIO("Test constructor of the class")
{
	GIVEN("An object of Population class")
	{
		Population p;
		THEN("Rules should be set to null");
		{
			REQUIRE(p.getEvenRule() == nullptr);
			REQUIRE(p.getOddRule() == nullptr);
		}
	}
}
SCENARIO("Test the initiatePopulation method")
{
	GIVEN("An object of Population class")
	{
		Population p;
		WHEN("Passing invalid values to initiatePopulation")
		{
			p.initiatePopulation("!!!","!!!");
			THEN("Both odd and even rules should be Conway")
			{
				REQUIRE(p.getEvenRule()->getRuleName() == "conway");
				REQUIRE(p.getOddRule()->getRuleName() == "conway");
			}
		}
		WHEN("Passing only one value to initiatePopulation")
		{
			p.initiatePopulation("von_neumann");
			THEN("odd and even rules must be equal")
			{
				REQUIRE(p.getEvenRule()->getRuleName() == p.getOddRule()->getRuleName());
			}
		}
		WHEN("Global filename is empty and initiatePopulation is called")
		{
			fileName = "";
			p.initiatePopulation("von_neumann");
			THEN("The random population is generated and getTotalCellPopulation should return greater than zero")
			{
				REQUIRE(p.getTotalCellPopulation() > 0);
			}
		}
		WHEN("Global filename is an invalid path and initiatePopulation is called")
		{
			fileName = "!!INVALID!!";
			THEN("An exception should be thrown")
			{
				REQUIRE_THROWS(p.initiatePopulation("conway"));
			}
			fileName = "";
		}
		WHEN("Global filename is a valid path and initiatePopulation is called")
		{
			fileName = "Population_Seed.txt";
			p.initiatePopulation("conway");
			THEN("The population should have cells")
			{
				REQUIRE(p.getTotalCellPopulation() > 0);
			}
			fileName = "";
		}
	}
}
SCENARIO("Test the calculateNewGeneration method")
{
	GIVEN("An object of Population class with initial random population")
	{
		Population p;
		fileName = "";
		p.initiatePopulation("conway");
		WHEN("Calling the calculateNewGeneration method")
		{
			int gen = p.calculateNewGeneration();
			THEN("generations should be 1")
			{
				REQUIRE(gen == 1);
			}
		}
	}
}