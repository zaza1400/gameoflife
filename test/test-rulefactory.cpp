/**
* file        test-cell.cpp
* author      zaza1400
* date        November 2018
* version     1.0
**/

#include <string>
#include "catch.hpp"
#include "GoL_Rules\RuleFactory.h"


SCENARIO("Test the factory pattern")
{
	GIVEN("Factory object and cells map")
	{
		auto fac = RuleFactory::getInstance();
		map<Point, Cell> cells;
		WHEN("passing valid rule to createAndReturnRule")
		{
			auto r = fac.createAndReturnRule(cells, "erik");
			THEN("The returned object should be of the we chose")
			{
				REQUIRE(r->getRuleName() == "erik");
			}
		}
		WHEN("passing invalid rule to createAndReturnRule")
		{
			auto r = fac.createAndReturnRule(cells, "!!NO_RULE!!");
			THEN("The returned object should be default to conway")
			{
				REQUIRE(r->getRuleName() == "conway");
			}
		}
		WHEN("passing no rule to createAndReturnRule")
		{
			auto r = fac.createAndReturnRule(cells);
			THEN("The returned object should be default to conway")
			{
				REQUIRE(r->getRuleName() == "conway");
			}
		}

	}
}