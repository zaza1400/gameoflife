/**
* file        test-cell.cpp
* author      zaza1400
* date        November 2018
* version     1.0
**/

#include <string>
#include "catch.hpp"
#include "GoL_Rules/RuleOfExistence_Conway.h"
#include "GoL_Rules/RuleOfExistence_Erik.h"
#include "GoL_Rules/RuleOfExistence_VonNeumann.h"

SCENARIO("Test the constructor of RuleOfExistence_Conway")
{
	GIVEN("A cell map and an instance of the conway's rule")
	{
		map<Point, Cell> cells;

		RuleOfExistence_Conway cr(cells);
		THEN("It should have the correct population limits")
		{
			REQUIRE(cr.getPopulationLimits().UNDERPOPULATION == 2);
			REQUIRE(cr.getPopulationLimits().OVERPOPULATION == 3);
			REQUIRE(cr.getPopulationLimits().RESURRECTION == 3);
		}
		THEN("The name should be 'conway'")
		{
			REQUIRE(cr.getRuleName() == "conway");
		}
		THEN("The number of directions should be 8")
		{
			REQUIRE(cr.getDirectionsCount() == 8);
		}
	}
}

SCENARIO("Test the executeRule method for Conway's rules")
{
	GIVEN("A conway rule and a sample population")
	{
		map<Point, Cell> cells;

		for (int i = 0; i < 5; i++)
		{
			for (int j = 0; j < 5; j++)
			{
				if (i == 0 || i == 4 || j == 0 || j == 4)
				{
					cells[Point{ i, j }] = Cell(true);//create a rim cell
				}
				else
				{
					bool alive = (i == 1 && j == 1) || (i == 2 && j == 1) || (i == 1 && j == 3) || (i == 2 && j == 3) || (i == 3 && j == 3) || (i == 2 && j == 3);
					if (alive)
						cells[Point{ i, j }] = Cell(false,ACTION::GIVE_CELL_LIFE);
					else
						cells[Point{ i, j }] = Cell(false);
				}
			}
		}

		RuleOfExistence_Conway cr(cells);
		WHEN("Executing the rule and updating state of cells")
		{
			cr.executeRule();
			THEN("We should check the state of population agains the expected state")
			{
				REQUIRE((cells[Point{ 1, 1 }].getNextGenerationAction() == ACTION::KILL_CELL));
				REQUIRE((cells[Point{ 1, 3 }].getNextGenerationAction() == ACTION::KILL_CELL));
				REQUIRE((cells[Point{ 2, 1 }].getNextGenerationAction() == ACTION::KILL_CELL));
				REQUIRE((cells[Point{ 3, 3 }].getNextGenerationAction() == ACTION::KILL_CELL));
				REQUIRE((cells[Point{ 3, 2 }].getNextGenerationAction() == ACTION::GIVE_CELL_LIFE));
				REQUIRE((cells[Point{ 2, 3 }].getNextGenerationAction() == ACTION::IGNORE_CELL));
				for (int i = 0; i < 5; i++)
				{
					for (int j = 0; j < 5; j++)
					{
						if ((i == 1 && j == 1) || (i == 1 && j == 3) || (i == 2 && j == 1) || (i == 3 && j == 3) || (i == 3 && j == 2) || (i == 2 && j == 3))
							continue;
						else
							REQUIRE((cells[Point{ i, j }].getNextGenerationAction() == ACTION::DO_NOTHING));
					}
				}
				
			}
			THEN("The next color of the cells should corespond to their next action")
			{
				for (int i = 0; i < 5; i++)
				{
					for (int j = 0; j < 5; j++)
					{
						auto action = cells[Point{ i, j }].getNextGenerationAction();
						auto& cell = cells[Point{ i, j }];
						cell.updateState();
						if (action == ACTION::KILL_CELL)
							REQUIRE((cell.getColor() == STATE_COLORS.DEAD));
						if (action == ACTION::GIVE_CELL_LIFE)
							REQUIRE((cell.getColor() == STATE_COLORS.LIVING));
					}
				}
			}
		}
	}
}
SCENARIO("Test the constructor of RuleOfExistence_Erik")
{
	GIVEN("A cell map and an instance of the Erik's rule")
	{
		map<Point, Cell> cells;

		RuleOfExistence_Erik er(cells);
		THEN("It should have the correct population limits")
		{
			REQUIRE(er.getPopulationLimits().UNDERPOPULATION == 2);
			REQUIRE(er.getPopulationLimits().OVERPOPULATION == 3);
			REQUIRE(er.getPopulationLimits().RESURRECTION == 3);
		}
		THEN("The name should be 'erik'")
		{
			REQUIRE(er.getRuleName() == "erik");
		}
		THEN("The number of directions should be 8")
		{
			REQUIRE(er.getDirectionsCount() == 8);
		}
	}
}

SCENARIO("Test the executeRule method for Erik's rules")
{
	GIVEN("A erik rule and a sample population")
	{
		map<Point, Cell> cells;

		for (int i = 0; i < 5; i++)
		{
			for (int j = 0; j < 5; j++)
			{
				if (i == 0 || i == 4 || j == 0 || j == 4)
				{
					cells[Point{ i, j }] = Cell(true);//create a rim cell
				}
				else
				{
					bool alive = (i == 1 && j == 1) || (i == 2 && j == 1) || (i == 1 && j == 3) || (i == 2 && j == 3) || (i == 3 && j == 3) || (i == 2 && j == 3);
					if (alive)
						cells[Point{ i, j }] = Cell(false,ACTION::GIVE_CELL_LIFE);
					else
						cells[Point{ i, j }] = Cell(false);
				}
			}
		}
		
		RuleOfExistence_Erik cr(cells);
		WHEN("Executing the rule and updating state of cells")
		{
			cr.executeRule();
			THEN("We should check the state of population agains the expected state")
			{
				REQUIRE((cells[Point{ 1, 1 }].getNextGenerationAction() == ACTION::KILL_CELL));
				REQUIRE((cells[Point{ 1, 3 }].getNextGenerationAction() == ACTION::KILL_CELL));
				REQUIRE((cells[Point{ 2, 1 }].getNextGenerationAction() == ACTION::KILL_CELL));
				REQUIRE((cells[Point{ 3, 3 }].getNextGenerationAction() == ACTION::KILL_CELL));
				REQUIRE((cells[Point{ 3, 2 }].getNextGenerationAction() == ACTION::GIVE_CELL_LIFE));
				REQUIRE((cells[Point{ 2, 3 }].getNextGenerationAction() == ACTION::IGNORE_CELL));
				for (int i = 0; i < 5; i++)
				{
					for (int j = 0; j < 5; j++)
					{
						if ((i == 1 && j == 1) || (i == 1 && j == 3) || (i == 2 && j == 1) || (i == 3 && j == 3) || (i == 3 && j == 2) || (i == 2 && j == 3))
							continue;
						else
							REQUIRE((cells[Point{ i, j }].getNextGenerationAction() == ACTION::DO_NOTHING));
					}
				}
				
			}
			THEN("The next color of the cells should corespond to their next action")
			{
				for (int i = 0; i < 5; i++)
				{
					for (int j = 0; j < 5; j++)
					{
						auto action = cells[Point{ i, j }].getNextGenerationAction();
						auto& cell = cells[Point{ i, j }];
						cell.updateState();
						if (action == ACTION::KILL_CELL)
							REQUIRE((cell.getColor() == STATE_COLORS.DEAD));
						if (action == ACTION::GIVE_CELL_LIFE)
							REQUIRE((cell.getColor() == STATE_COLORS.LIVING));
					}
				}
			}
		}
	}
}
SCENARIO("Test the constructor of RuleOfExistence_VonNeumann")
{
	GIVEN("A cell map and an instance of the Von Neumann's rule")
	{
		map<Point, Cell> cells;

		RuleOfExistence_VonNeumann vr(cells);
		THEN("It should have the correct population limits")
		{
			REQUIRE(vr.getPopulationLimits().UNDERPOPULATION == 2);
			REQUIRE(vr.getPopulationLimits().OVERPOPULATION == 3);
			REQUIRE(vr.getPopulationLimits().RESURRECTION == 3);
		}
		THEN("The name should be 'von_neumann'")
		{
			REQUIRE(vr.getRuleName() == "von_neumann");
		}
		THEN("The number of directions should be 4")
		{
			REQUIRE(vr.getDirectionsCount() == 4);
		}
	}
}

SCENARIO("Test the executeRule method for Von Neumann's rules")
{
	GIVEN("A Von Neumann rule and a sample population")
	{
		map<Point, Cell> cells;

		for (int i = 0; i < 5; i++)
		{
			for (int j = 0; j < 5; j++)
			{
				if (i == 0 || i == 4 || j == 0 || j == 4)
				{
					cells[Point{ i, j }] = Cell(true);//create a rim cell
				}
				else
				{
					bool alive = (i == 1 && j == 1) || (i == 2 && j == 1) || (i == 1 && j == 3) || (i == 2 && j == 3) || (i == 3 && j == 3) || (i == 2 && j == 3);
					if (alive)
						cells[Point{ i, j }] = Cell(false,ACTION::GIVE_CELL_LIFE);
					else
						cells[Point{ i, j }] = Cell(false);
				}
			}
		}
		
		RuleOfExistence_VonNeumann cr(cells);
		WHEN("Executing the rule and updating state of cells")
		{
			cr.executeRule();
			THEN("We should check the state of population agains the expected state")
			{
				REQUIRE((cells[Point{ 1, 1 }].getNextGenerationAction() == ACTION::KILL_CELL));
				REQUIRE((cells[Point{ 1, 3 }].getNextGenerationAction() == ACTION::KILL_CELL));
				REQUIRE((cells[Point{ 2, 1 }].getNextGenerationAction() == ACTION::KILL_CELL));
				REQUIRE((cells[Point{ 3, 3 }].getNextGenerationAction() == ACTION::KILL_CELL));
				REQUIRE((cells[Point{ 2, 3 }].getNextGenerationAction() == ACTION::IGNORE_CELL));
				for (int i = 0; i < 5; i++)
				{
					for (int j = 0; j < 5; j++)
					{
						if ((i == 1 && j == 1) || (i == 1 && j == 3) || (i == 2 && j == 1) || (i == 3 && j == 3) || (i == 2 && j == 3))
							continue;
						else
							REQUIRE((cells[Point{ i, j }].getNextGenerationAction() == ACTION::DO_NOTHING));
					}
				}
				
			}
			THEN("The next color of the cells should corespond to their next action")
			{
				for (int i = 0; i < 5; i++)
				{
					for (int j = 0; j < 5; j++)
					{
						auto action = cells[Point{ i, j }].getNextGenerationAction();
						auto& cell = cells[Point{ i, j }];
						cell.updateState();
						if (action == ACTION::KILL_CELL)
							REQUIRE((cell.getColor() == STATE_COLORS.DEAD));
						if (action == ACTION::GIVE_CELL_LIFE)
							REQUIRE((cell.getColor() == STATE_COLORS.LIVING));
					}
				}
			}
		}
	}
}
