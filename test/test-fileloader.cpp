/**
* file        test-cell.cpp
* author      zaza1400
* date        November 2018
* version     1.0
**/

#include <string>
#include "catch.hpp"
#include "Support\FileLoader.h"

SCENARIO("Test the loadPopulationFromFile method")
{
	GIVEN("An instance of FileLoader")
	{
		FileLoader f;
		map<Point, Cell> cells;
		WHEN("global fileName is points to an invalid file")
		{
			fileName = "!!INVALID_PATH!!";
			THEN("Method should throw")
			{
				REQUIRE_THROWS(f.loadPopulationFromFile(cells));
			}
			fileName = "";
		}
		WHEN("global fileName is points to a valid file")
		{
			fileName = "Population_Seed.txt";
			f.loadPopulationFromFile(cells);
			fileName = "";
			THEN("World size should be 20*10")
			{
				REQUIRE(WORLD_DIMENSIONS.WIDTH == 20);
				REQUIRE(WORLD_DIMENSIONS.HEIGHT == 10);
			}
			THEN("Total number of cells should be 20*10")
			{
				REQUIRE(cells.size() == 200);
			}
			THEN("Number of alive cells should be 23")
			{
				int count = 0;
				for (auto & k : cells)
				{
					if (k.second.getColor() == STATE_COLORS.LIVING)
						count++;
				}
				REQUIRE(count == 23);
			}
			THEN("Cell 0,0 should be a rim cell")
			{
				REQUIRE((cells[{0, 0}].isRimCell()));
			}
			THEN("Cell 19,0 should be a rim cell")
			{
				REQUIRE((cells[{19, 0}].isRimCell()));
			}
			THEN("Cell 0,9 should also be a rim cell")
			{
				REQUIRE((cells[{0, 9}].isRimCell()));
			}
			THEN("CELL 19,9 should also be a rim cell")
			{
				REQUIRE((cells[{19, 9}].isRimCell()));
			}
			
		}
	}
}

