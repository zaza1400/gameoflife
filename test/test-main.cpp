#define CATCH_CONFIG_MAIN
#include "catch.hpp"

/**
 * @page aboutTheAuthor About the author!
 * <b>Name:</b> Peter Johansson <br />
 * <b>Student ID:</b> pejo9700 <br />
 * <b>Course:</b> Methods and tools in Software development, 7.5hp <br />
 * <b>Assignment:</b> Lab 4 - Documentation with Doxygen <br />
 * <b>Email:</b> pejo9700@student.miun.se
 * @image html goto.png
 */

/**
 * @mainpage Stack project
 *
 * @section About Documentation with Doxygen
 * This project is part of the fourth lab in the course <i>Methods and tools in Software
 * development</i> at Mid Sweden University.<br />The goal of the lab was to create documentation
 * regarding a stack data structure, that was written in a previous lab, using Doxygen.<br /><br />
 * Here is some information about the project's author.
 * @ref aboutTheAuthor
 * @image html stack.jpg
*/