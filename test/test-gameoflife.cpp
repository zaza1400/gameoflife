
/*
 * @file test-gameoflife.cpp
 * @author Fredrik Arhusiander
 * @date 2018-11-01
 * @version 0.1
 */

#include "catch.hpp"
#include "GameOfLife.h"


SCENARIO("Test the GameOfLife class")
{
    GIVEN("An instance of GameOfLife with wrong arguments")
	{
        GameOfLife gol(-10, "simon", "simon");
        THEN("runSimulation() should throw an exception")
		{
            REQUIRE_THROWS(gol.runSimulation());
        }
    }
}