/**
* file        test-cell.cpp
* author      zaza1400
* date        November 2018
* version     1.0
**/

#include <string>
#include "catch.hpp"
#include "Support\MainArguments.h"
#include "Support\MainArgumentsParser.h"

SCENARIO("Test the help argument")
{
	GIVEN("An instance of ApplicationValues and an instance of MainArgumentsParser")
	{
		ApplicationValues tv;
		MainArgumentsParser parser;
		char * argv[] = { "-h" };
		WHEN("Calling the runParser method")
		{
			tv = parser.runParser(argv, 1);
			THEN("Run simulation should be false")
			{
				REQUIRE_FALSE(tv.runSimulation);
			}
		}
	}
}
SCENARIO("Test the generations argument")
{
	GIVEN("An instance of ApplicationValues and an instance of MainArgumentsParser")
	{
		ApplicationValues tv;
		MainArgumentsParser parser;
		char * argv[] = { "-g" ,"10" };
		WHEN("Calling the runParser method")
		{
			tv = parser.runParser(argv, 2);
			THEN("Max generations should be 10")
			{
				REQUIRE(tv.maxGenerations==10);
			}
		}
	}
}

SCENARIO("Test the world size argument")
{
	GIVEN("An instance of ApplicationValues and an instance of MainArgumentsParser")
	{
		ApplicationValues tv;
		MainArgumentsParser parser;
		char * argv[] = { "-s", "15*10" };
		WHEN("Calling the runParser method")
		{
			tv = parser.runParser(argv, 2);
			THEN("WORLD_DIMENSIONS.WIDTH should be 15 and WORLD_DIMENSIONS.HEIGHT should be 10")
			{
				REQUIRE(WORLD_DIMENSIONS.WIDTH==15);
				REQUIRE(WORLD_DIMENSIONS.HEIGHT==10);
			}
		}
	}
}
SCENARIO("Test the file argument")
{
	GIVEN("An instance of ApplicationValues and an instance of MainArgumentsParser")
	{
		ApplicationValues tv;
		MainArgumentsParser parser;
		char * argv[] = { "-f", "Population_Seed.txt" };
		WHEN("Calling the runParser method")
		{
			tv = parser.runParser(argv, 2);
			THEN("The global fileName variable should have the value we entered.")
			{
				REQUIRE(fileName == "Population_Seed.txt");
			}
		}
	}
}

SCENARIO("Test the even rule argument")
{
	GIVEN("An instance of ApplicationValues and an instance of MainArgumentsParser")
	{
		ApplicationValues tv;
		MainArgumentsParser parser;
		char * argv[] = { "-er", "erik" };
		WHEN("Calling the runParser method")
		{
			tv = parser.runParser(argv, 2);
			THEN("The even rule name should be erik")
			{
				REQUIRE(tv.evenRuleName=="erik");
			}
		}
	}
}

SCENARIO("Test the odd rule argument")
{
	GIVEN("An instance of ApplicationValues and an instance of MainArgumentsParser")
	{
		ApplicationValues tv;
		MainArgumentsParser parser;
		char * argv[] = { "-or", "erik" };
		WHEN("Calling the runParser method")
		{
			tv = parser.runParser(argv, 2);
			THEN("The odd rule name should be erik")
			{
				REQUIRE(tv.oddRuleName== "erik");
			}
		}
	}
}